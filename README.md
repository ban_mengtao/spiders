Crawl_News：爬取网易，CCTV，财经网新闻内容。intellgent_parse.py负责统一解析网页源代码，提取新闻内容。

car_58：爬取58同城二手车的数据。callback.js生成请求链接中的callback参数；car_verify.js获取验证码滑动轨迹的加密数据；car_verify.py携带滑动轨迹加密数据发送请求完成校验；city.py 58二手车所有的城市列表；log_write.py日志书写模块；download_v5.py负责爬取，解析，验证。

mtspider：爬取美团美食数据。meituan_city.txt提前提取的美团所有城市链接和城市名；meituan.js负责获取token值。

jd:爬取京东商品数据。jd.txt导航栏的网页源代码，包含所有商品分类的链接；jd_thread.py负责解析提取保存数据；jdspiderlog.py日志书写模块；puppeteer_down.js使用puppeteer发送请求渲染数据。

taobao:爬取淘宝商品数据。api_method.py负责构造链接从h5接口获取数据解析保存；getkey.py负责登录后获取所有详细分类关键字；tb.js获取密码加密数据；tb_token.js模拟生成h5接口sign的值。