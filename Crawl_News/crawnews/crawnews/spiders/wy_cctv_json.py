# -*- coding: utf-8 -*-
import scrapy
from intellgent_parse import News
import json
import re
import pymongo


class NewsSpider(scrapy.Spider):

    name = 'wy_cctv'
    allowed_domains = ['cctv.com', '163.com']
    new = News()
    conn = pymongo.MongoClient(host='localhost', port=27017)
    collection = conn.news_url.url

    def start_requests(self):
        """从mongodb中获取url，发送请求"""
        mongo_finded = NewsSpider.collection.find()
        for i in mongo_finded:
            yield scrapy.Request(i['url_'], method='GET', callback=self.parse)

    def parse(self, response):
        """对相应网页进行解析，提取新闻内容"""
        html = response.text
        pattern = re.compile(r'.*?(\[.+\])', re.S)
        html = json.loads(re.search(pattern, html).group(1))
        for i in html:
            try:
                href = i['url']
            except:
                href = i['docurl']
            finally:
                print('正在爬取：',href)
                newses = NewsSpider.new.main(href,8)
                if newses:
                    yield {'news_content':newses}
  

 
