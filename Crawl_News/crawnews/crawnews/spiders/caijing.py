# -*- coding: utf-8 -*-
import scrapy
from intellgent_parse import News
import re


class CaijingSpider(scrapy.Spider):

    name = 'caijing'
    allowed_domains = ['caijing.com.cn']
    intell_parse = News()

    def start_requests(self):
        start_urls = ['http://caijing.com.cn/']#,'https://www.tiexue.net/']
        for url in start_urls:
            yield scrapy.Request(url, callback=self.parse)
        # for url in self.generateurl():
        #     yield scrapy.Request(url, callback=self.parse)

    def generateurl(self):
        url_template = 'https://www.tiexue.net/xmlhttp/GetDefaultv3JsonData.aspx?op={op}&pageindex={page}'
        for o in range(1, 7):
            for i in range(1, 1600):
                yield url_template.format(op=o, page=i)

    def parse(self, response):
        """匹配出新闻链接，再发送请求，同时从网页中提取新闻内容"""
        news_url = re.findall(r'<a href="(.*?)html" .*?<\/a>', response.text)
        try:
            navigator_urls = response.css(
                'ul.naviga1 li a::attr(href)').extract()
        except:
            pass
        if navigator_urls:
            for u in navigator_urls:
                yield scrapy.Request(u, callback=self.parse)

        for nu in news_url:
            print('正在爬取：', nu+'html')
            newses = CaijingSpider.intell_parse.main(nu+'html',8)
            if newses:
                yield {'news_content':newses}
