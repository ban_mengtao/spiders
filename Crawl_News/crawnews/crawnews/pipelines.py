# -*- coding: utf-8 -*-
import pymysql
from hashlib import md5
# Define your item pipelines here
from pybloom_live import BloomFilter
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


class CrawnewsPipeline(object):

    BFN = BloomFilter(capacity=300000,error_rate=0.01) 
    def open_spider(self,spider):
        self.client = pymysql.connect(host='localhost',user='root',port=3306,password='******',db='test')
        self.cursor = self.client.cursor()
        self.sql = 'insert into news(news_content,md5) values("{newses}","{md}")'


    def process_item(self, item, spider):

        m = md5(item['news_content'].encode('utf-8'))
        md_value = m.hexdigest()
        if md_value not in CrawnewsPipeline.BFN:
            CrawnewsPipeline.BFN.add(md_value)
            try:
                self.cursor.execute(self.sql.format(newses=item['news_content'],md=md_value))
                self.client.commit()
            except:
                self.client.rollback()
        return item
    
    def close_spider(self,spider):
        
        self.client.close()
