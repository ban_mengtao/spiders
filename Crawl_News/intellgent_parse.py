﻿# coding:utf-8
import re
import requests
from html import unescape
from pybloom_live import BloomFilter
from lxml.html import HtmlElement, fromstring, etree



class News:
    BF = BloomFilter(capacity=300000,error_rate=0.01)
    HEADERS = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36"
    }
    DELETE_TAG = ["img","select", "form", "style", "script", "link", "video", "iframe", "source", "picture", "header", "blockquote", "head",
                  "input", "aside"]
    DELETE_CLASS = ["reply", "footer", "menu", "BodyEnd", "AboutCtrl", "Xgzx", "card", "user-info", "yuedu", "pinglun", "shoucang", "js-font", "copyright", "rightbox_title", "interested-article-box", "z_cen_wntj", "rdph", "pdtj", "comment", "nav", "news-links", "gives",
                    "news-key", "hdear", "exceptional-box", "z_cen_sq", "news-push", "search", "machineContainer", "weixinloginedAlert",
                    "qrcode", "pagebottom", "Jdzt", "tuijian", "jump", "up_down", "login", "layer-reprinted", "swiper-slide", "fr nright", "foot", "mzsms", "BottomBox", "history", "hot_fund", "fund_tab_cut", "article-list", "tjbox", "foucs", "textPbl"
                    ]
    DELETE_ID = [
        "footer",
        "copyright"
    ]

    def iter_node(self, element):
        """递归遍历节点"""
        yield element
        for sub_element in element:
            if issubclass(type(sub_element), HtmlElement):
                yield from self.iter_node(sub_element)
                
    def handle_html(self, element):
        """处理网页内容"""
        for node in self.iter_node(element):
            if not node.text and not node.getchildren(): # 删除没有文本属性和没有子节点的标签
                node.drop_tree()
            if node.tag.lower() == "div": # 删除没有子节点和实质性内容的div标签
                if node.text and not len(node.getchildren()) and len(node.text.strip()) < 30:
                    node.drop_tree()
            if node.tag.lower() == "span": # 将有实质性内容的span标签转为p标签后面统一处理
                if node.text and node.text.strip() and not node.getchildren():
                    node.tag = "p"
            if node.tag.lower() == 'p': # 将没有实质性内容的p标签删除
                if not (node.text and node.text.strip()):
                    parent = node.getparent()
                    if parent is not None:
                        node.drop_tag()

            class_name = node.get('class', '') # 将class属性含有特殊关键字的标签删除
            if class_name:
                for attribute in News.DELETE_CLASS:
                    if attribute.lower() in class_name.lower():
                        parent = node.getparent()
                        if parent is not None:
                            parent.remove(node)
                        break
            id_name = node.get("id") # 将id属性含有特殊关键字的标签删除
            if id_name:
                for attribute in News.DELETE_ID:
                    if attribute.lower() in id_name.lower():
                        parent = node.getparent()
                        if parent is not None:
                            parent.remove(node)
                        break

            if node.tag.lower() == "ul": # 将有多个li标签的ul删除
                li_num = len(node.xpath(".//li"))
                for _ in node.getchildren():
                    if _.xpath('.//a') and li_num >= 2:
                        parent = node.getparent()
                        if parent is not None:
                            parent.remove(node)
                        break

        handled_html = str(element.text_content())
        handled_html = handled_html.replace(' ', '').replace('\t', '')
        return handled_html

    def down_html(self, url):
        """下载网页,并获取网页编码方式，进行编码"""
        if url not in News.BF:
            News.BF.add(url)
            html = requests.get(url, headers=News.HEADERS,timeout=10)
            app_encode = html.apparent_encoding
            html.encoding = app_encode
            if html.status_code//100==2:
                html = html.text
                return html
        return None

    @classmethod
    def continus_find(cls, num_list, line_threshold):
        """获得网页内容字符串后，将网页内容按照换行符切割。新闻内容都比较集中，
        段落之间一般不会超过line_threshold个换行符，将换行符在line_threshold个以内的行索引取出。"""
        s = 1
        find_list = []
        have_list = []
        while s <= len(num_list)-1:
            if num_list[s]-num_list[s-1] < line_threshold:
                flag = s-1
                while num_list[s]-num_list[s-1] < line_threshold:
                    s += 1
                    if s == len(num_list):
                        break
                find_list.append(num_list[flag:s])
                have_list += num_list[flag:s]
            else:
                s += 1
        # 判断，是否找到符合上述（换行符在line_threshold个以内的段落）要求的列表
        if find_list:
            flag = 0
            for i in range(len(find_list)):
                if (i+1) < len(find_list):
                    lmax = len(find_list[i])
                    if lmax < len(find_list[i+1]):
                        lmax = find_list[i+1]
                        flag = i+1
                else:
                    return find_list[flag]
        else:
            return num_list

    def get_news(self, handled_html,line_threshold):
        str_list = []
        news_str=[]
        str_split = handled_html.splitlines()
        # print(str_split)
        for i, l in enumerate(str_split):
            l = l.strip()
            if l and len(l) > 30: # 过滤掉不到30个字符的行，新闻一般都会超过30个字
                str_list.append(i)
        res = News.continus_find(str_list,line_threshold)
        try:
            for i in res:
                news_str.append(str_split[i])
            return ''.join(news_str)
        except Exception as e:
            print(e)

    def main(self, url,line_threshold):
        html = self.down_html(url)

        if html:
            html = re.sub('</?br.*?>', '', html)
            element = fromstring(html)
            etree.strip_elements(element, *News.DELETE_TAG) # 删除指定标签
            handled_html = self.handle_html(element)
            news_str = self.get_news(handled_html,line_threshold)
            return news_str
        else:
            return None


if __name__ == "__main__":

    url = 'https://news.cctv.com/2020/08/29/ARTIuBmvwRTYAF1tqRcrqXqV200829.shtml?spm=C96370.PsikHJQ1ICOX.Em32AuyOHUeL.13'
    new = News()
    news_strs = new.main(url,8)
    print(news_strs)
