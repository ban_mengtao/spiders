const puppeteer = require('puppeteer')

async function down_html(url){

    const browser = await puppeteer.launch({
        headless:true
    })
    const page = await browser.newPage()
    await page.goto(url)
    // 将页面滑动到底部
    await page.evaluate(() => {
        window.scrollTo(0,10000)
    })
    await page.waitFor(900)
    const sku = await page.$$eval('#J_goodsList li', divs => divs.map(div => div.getAttribute('data-sku')))
    console.log(await page.content())
    // console.log(sku)
    await browser.close()

}

var url = process.argv[2]
down_html(url)