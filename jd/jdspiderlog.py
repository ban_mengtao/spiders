import logging
import getpass
import sys

class JDLog():

    __instance = None
    def __new__(cls):
        if cls.__instance is None:
            cls.__instance = super(JDLog, cls).__new__(cls)
        return cls.__instance
    
    def __init__(self):
        self.user = getpass.getuser()
        self.logger = logging.getLogger(self.user)
        self.logger.setLevel(logging.INFO)
        self.log_file = sys.argv[0][0:-3] + '.log'
        self.formatter = logging.Formatter('%(asctime)-12s %(levelname)-8s %(name)-10s %(message)-12s\r\n')

        self.handle = logging.FileHandler(self.log_file, encoding='utf-8')
        self.handle.setLevel(logging.INFO)
        self.handle.setFormatter(self.formatter)

        self.logger.addHandler(self.handle)
    
    def log_write(self, msg):
        self.logger.info(msg)


