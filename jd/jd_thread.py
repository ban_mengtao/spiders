import requests
import re
from bs4 import BeautifulSoup
from time import sleep
import redis
from functools import wraps
import pymysql
import subprocess
from concurrent.futures import ThreadPoolExecutor
from DBUtils.PooledDB import PooledDB, SharedDBConnection
from DBUtils.PersistentDB import PersistentDB, PersistentDBError, NotSupportedError
from jdspiderlog import JDLog


def retry(delay=0, num_retry=3):
    """
    负责重试
    :param delay: 延长的时间
    :param num_retry: 重试次数
    """
    def mid(f):
        @wraps(f)
        def wrap_f(self, *args, **kwargs):
            i = 0
            while i < num_retry:
                try:
                    sleep(delay)
                    html = f(self, *args, **kwargs)
                    return html
                except:
                    self.jd_log.log_write('第{}次重新爬取{}'.format(i,*args))
                    i = i + 1
        return wrap_f
    return mid


class JDSpider():

    def __init__(self):
        """
        :param self.jd_log: 初始化日志类
        :param self.CONFIG: 数据库配置信息
        :param self.RED: 初始化redis
        :param self.db_pool: 初始化数据库连接池
        :param self.SQL: 详情页包含评价的sql插入语句
        :param self.SQL_I: 详情页不包含评价的sql插入语句
        :param self.pool: 开启三个线程
        """
        self.jd_log = JDLog()
        self.CONFIG = {
        'host': 'localhost',
        'port': 3306,
        'database': 'taobao',
        'user': 'root',
        'password': '******'
                        }
        self.RED = redis.Redis(host='localhost', port=6379)
        self.db_pool = self.get_db_pool(True)
        self.CONNECT = self.db_pool.connection()
        self.CURSOR = self.CONNECT.cursor()
        self.SQL = 'insert into jd_goods(name,price,label,sales,shop) value(%s,%s,%s,%s,%s)'
        self.SQL_I = 'insert into jd_goods(name,price,label,shop) value(%s,%s,%s,%s)'
        self.pool = ThreadPoolExecutor(max_workers=3)

    def get_db_pool(self, is_mult_thread):
        """构建数据库连接池"""
        if is_mult_thread:
            poolDB = PooledDB(
                creator=pymysql,
                maxconnections=3,
                mincached=2,
                maxcached=5,
                maxshared=3,
                blocking=True,
                setsession=[],
                ping=0,
                **self.CONFIG
            )
        else:
            poolDB = PersistentDB(
                creator=pymysql,
                maxusage=1000,
                **self.CONFIG
            )
        return poolDB
    
    def run(self):
        cat_url = self.get_cat() # 获取京东主页导航栏商品连接
        self.save_start_to_redis(cat_url)
        self.main()
        self.RED.close()
        self.CONNECT.close()

    def main(self):
        url = self.get_url()
        while url:
            url = url.decode()
            try:
                done = self.pool.submit(self.puppereer_down, url)
                done.add_done_callback(self.parse)

            except Exception as e:
                self.jd_log.log_write('下载失败  {}'.format(e))
            url = self.get_url()
    
    def parse(self, future):
        """线程池回调函数，京东商品网址有两种，一种是list.jd.com,使用
        parse_goods方法解析；另一种是i-list.jd.com使用parse_i_goods
        解析"""
        result_callback = future.result()
        if result_callback:
            html = result_callback[0]
            url = result_callback[1]
        else:
            self.jd_log.log_write('result_callback为None')
            html = None
        
        try:
            if html:
                if url.startswith('https://i-'):
                    self.parse_i_goods(html, url)
                else:
                    self.parse_goods(html, url)
            else:
                self.jd_log.log_write('html为空')
        except Exception as e:
            self.jd_log.log_write('解析失败  {}'.format(e))

    def parse_goods(self, html, url):
        """提取数据的具体方法，返回comments: 评价数，price: 价格，
        names: 商品名，shops: 店铺，labels: 商品标签（放心购，自营等）
        """
        soup = BeautifulSoup(html, 'lxml')
        comments = map(lambda x: x.get_text().strip(), soup.select(
            'div[id*="goodsList"] div[class*="p-commit"]'))
        price = map(lambda x: x.get_text().strip(), soup.select('.p-price strong'))
        names = map(lambda x: x.get_text().strip(), soup.select(
            'div[id*="goodsList"] div[class*="p-name"] a em'))
        shops = map(lambda x: x.get_text().strip(), soup.select(
            'div[id*="goodsList"] div[class*="p-shop"]'))
        labels = map(lambda x: x.get_text().strip().replace('\n','_'), soup.select(
            'div[id*="goodsList"] div[class*="p-icons"]'))
        if url.endswith('=1'):
            # 提取该类商品一共有多少页，存入redis
            pages = soup.select('span[class*="fp-text"] i')[0].get_text().strip()
            self.save_new_to_redis(url, pages)
        try:
            data = list(zip(names, price, labels, comments, shops))
        except Exception as e:
            self.jd_log.log_write('map压缩失败  {}'.format(e))

        self.save_to_sql(self.SQL, data)

    def parse_i_goods(self, html, url):
        """提取数据的具体方法，price: 价格，names: 商品名，shops:
        店铺，labels: 商品标签（放心购，自营等）
        """
        soup = BeautifulSoup(html, 'lxml')
        names = map(lambda x: x.get_text().strip(), soup.select(
            'div[class*="p-name"] a em'))
        labels = map(lambda x: x.get_text().strip().replace('\n','_'), soup.select(
            'div[class*="p-icons"]'))
        shops = map(lambda x: x.get_text().strip(), soup.select(
        'div[class*="p-shop"]'))
        prices = map(lambda x: x.get_text().strip(), soup.select('.p-price strong'))
        if url.endswith('=1'):
            # 提取该类商品一共有多少页，存入redis
            pages = soup.select('span[class*="fp-text"] i')[0].get_text().strip()
            self.save_new_to_redis(url, pages)
        try:
            data = list(zip(names, prices, labels, shops))
        except Exception as e:
            self.jd_log.log_write('存储失败  {}'.format(e))
        self.save_to_sql(self.SQL_I, data)

    def save_to_sql(self, sql, data):
        try:
            self.CURSOR.executemany(sql, data)
            self.CONNECT.commit()
        except Exception as e:
            self.jd_log.log_write('存储失败  {}'.format(e))
            self.CONNECT.rollback()

    def save_start_to_redis(self, cat_url):
        for cat in cat_url[:3]:
            url_goods = f'https://{cat}&page=1'
            self.RED.sadd('jd_url', url_goods)

    def save_new_to_redis(self, url, pages):
        for i in range(2, int(pages)):
            new = url.replace('page=1', 'page=' + str(i))
            self.RED.sadd('jd_url', new)

    @retry()
    def puppereer_down(self, url):
        """
        使用puppeteer请求页面,以字符串形式返回网页源代码
        """
        self.jd_log.log_write('正在爬取{}'.format(url))
        url_ = url.replace('&', '^&')
        sleep(1)
        ret = subprocess.Popen(f'node puppeteer_down.js {url_}',
                            shell=True,
                            stdout=subprocess.PIPE
                            )
        stdout, stderr = ret.communicate(timeout=8)
        html = stdout.decode(encoding='utf-8')
        return [html,url]

    def get_url(self):
        url = self.RED.spop('jd_url')
        return url

    def get_cat(self):
        with open('jd.txt', 'r', encoding='utf-8') as f:
            cat_url_pattern = re.compile(
            r'"n":"(.{3,10}\.jd\.com/list\.html\?cat=.*?)\|', re.S)
            content = f.read()
            cat_url = re.findall(cat_url_pattern, content)
            return cat_url


if __name__ == "__main__":
    jd_spider = JDSpider()
    jd_spider.run()
    
    
