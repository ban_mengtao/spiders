# -*- coding: utf-8 -*-
import scrapy
import re
import os
import json
import requests
import pickle
import logging
import time


class MtCrawlSpider(scrapy.Spider):
    name = 'mt_crawl'
    allowed_domains = ['meituan.com']

    @staticmethod
    def get_uuid(href):
        """
        :param href:待爬取网页的url，例如：https://as.meituan.com/meishi
        :return uuid:生成token所需的uuid
        """
        headers_uuid = {
        'Host': 'bj.meituan.com',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
                        }
        resp_uuid = requests.get(href ,headers=headers_uuid).text
        uuid = re.findall(r"uuid: '(.*?)',", resp_uuid, re.S)[0]
        return uuid


    @staticmethod
    def get_url(url_prefix=None, cityName=None, page=None, uuid=None, url_all=None):
        """
        :param url_prefix:待爬取网页的url，例如：https://as.meituan.com/meishi
        :param cityName:要爬取数据的城市名,例如：北京
        :param page:要爬取的第几页数据
        :param uuid:网址中的uuid信息
        :param url_all:由url_prefix,cityName,page,uuid构建的完整链接，用于生成token
        :return url:获取数据的接口链接
        """
        obj_replace = {'/':'%2F','+':'%2B','=':'%3D'}
        if url_all:
            url_base = url_all
        else:
            url_base = f'{url_prefix}api/poi/getPoiList?cityName={cityName}&cateId=0&areaId=0&sort=&dinnerCountAttrId=&page={page}&userId=&uuid={uuid}&platform=1&partner=126&originUrl=https%3A%2F%2Fbj.meituan.com%2Fmeishi%2F&riskLevel=1&optimusCode=10'
        url_argv = url_base.replace('&','^&')
        ret = os.popen(f'node meituan.js {url_argv}')
        res = ret.readlines()

        token = re.findall(r'\[(.*?)\]', str(res), re.S)[0]
        _token = token.strip('\'')[:-2]
        for k,v in obj_replace.items():
            _token = _token.replace(k,v)
        _token = f'&_token={_token}'
        url = url_base + _token
        return url

    def start_requests(self):
        # 从pipeline中声明的cities_dict中读取城市名和对应链接
        for city in self.cities_dict[:1]:
            for name, href in city.items():
                # 可能有些城市的美团链接无法访问，比如https://arongqi.meituan.com/，通过try直接跳过
                try:
                    uuid = MtCrawlSpider.get_uuid(href)
                    url = MtCrawlSpider.get_url(href, name, 1, uuid)
                    logging.info('正在爬取{}'.format(url))  
                    headers = {
                            'Host': href[9:-8],
                            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
                            'Referer': href,
                            }
                    time.sleep(2)
                    yield scrapy.Request(url, headers=headers,meta={'add_page':True,'headers_':headers,'retry_num':0})
                except:
                    logging.info('start_request错误', href)
                    continue
                
                   


    def parse(self, response):
        """提取店铺地址（addr），人均价格（price），评分（score），
        评论数（comment），店名（title），数据总数（pages）信息，
        默认start_request只请求每个城市第一页数据，这里获取数据总
        数后按每页15条数据分页，再生成url发送请求"""
        
        try:
            html = json.loads(response.text)
            pages = html['data']['totalCounts']
            poiInfos = html['data']['poiInfos']
            addr = map(lambda x: x['address'], poiInfos)
            price = map(lambda x: x['avgPrice'], poiInfos)
            score = map(lambda x: x['avgScore'], poiInfos)
            comment = map(lambda x: x['allCommentNum'], poiInfos)
            title = map(lambda x: x['title'], poiInfos)
            data_zip = list(zip(addr, price, score, comment, title))
            data = list(map(lambda x: {'addr':x[0], 'price':x[1], 'score':x[2], 'comments':x[3], 'title':x[4]}, data_zip))

            yield {'data':data} 
        except:
            logging.warning('爬取失败', response.url)

        if response.meta['add_page']:
            
            pages = pages//15
            headers=response.meta['headers_']
            for i in range(1,pages):
                url_all = response.url.split('&_token')[0].replace('page=1','page={}'.format(i))
                url = MtCrawlSpider.get_url(url_all=url_all)
                logging.info('正在爬取{}'.format(url))  
                yield scrapy.Request(url, headers=headers,meta={'add_page':False,'headers_':headers,'retry_num':0},callback=self.parse)
   
        
