# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
import pickle
import pymongo
import logging


class MtspiderPipeline(object):

    config = {
        'host':'192.168.1.13',
        'port':27017,
        'database':'test'
    }

    def process_item(self, item, spider):
        
        self.collection.insert_many(item['data'])
        return item
    
    
    def open_spider(self, spider):
        """meituan_city.txt存储所有城市名，
        及对应的美团链接，绑定到spider上"""
        logging.info('爬虫启动')
        self.db = pymongo.MongoClient(host='localhost',port=27017)
        self.collection = self.db.test.mt
        spider.f = open('./mtspider/meituan_city.txt','rb')
        spider.cities_dict = pickle.load(spider.f)

    def close_spider(self, spider):
        spider.f.close()
        logging.info('爬虫已关闭')
    
    
