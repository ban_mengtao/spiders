import requests
import time
from PIL import Image
import aircv as ac
import os
import re
import json


def save_captcha():
    """保存验证码图片，并返回session_id, response_id"""
    headers_captcha = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
    'accept': 'image/webp,image/apng,image/*,*/*;q=0.8',
    'accept-encoding': 'gzip, deflate, br',
    'accept-language': 'zh-CN,zh;q=0.9,en;q=0.8',
    'cookie': 'id58=c5/nfF8oqage5rsmEt8aAg==; 58home=tj; city=tj; 58tj_uuid=f0606c8f-bfe0-4918-a05c-54792979825d; als=0; wmda_new_uuid=1; wmda_uuid=8d04e2a5433c9b87989e8dd97f0910d7; xxzl_deviceid=hk4tMTLXOoZDMq1OWS%2FbYdQsr1%2F6tJGVp6JnDKghAebk%2BWYpclsNs7e5ckbFgJ6m; wmda_visited_projects=%3B11187958619315%3B1732038237441; myLat=""; myLon=""; mcity=tj; new_uv=5; xxzl_cid=279a85402ac4446f9b35cada3a75d9e8; xzuid=32ea41f7-b931-49f6-bda8-2b622227d207',
    'referer': 'https://callback.58.com/antibot/verifycode?serialId=52e2ca88845f157e29a6d26349ef0344_6a059b1477814bb8baf7ee04e2b61764&code=22&sign=235448960cb8f4d6cd710b06c61dc57a&namespace=usdt_infolist_car&url=https%3A%2F%2Finfo5.58.com%3A443%2Ftj%2Fershouche%2F%3FPGTID%3D0d100000-0001-2e0e-cd43-870ea87150b4%26ClickID%3D8',
    'sec-fetch-dest': 'image'
    }
    headers_session = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36'
    }

    data_session = {
        'serialId': '52e2ca88845f157e29a6d26349ef0344_6a059b1477814bb8baf7ee04e2b61764',
        'code': '22',
        'sign': '235448960cb8f4d6cd710b06c61dc57a',
        'url': 'https://info5.58.com:443/tj/ershouche/?PGTID=0d100000-0001-2e0e-cd43-870ea87150b4&ClickID=8',
        'namespace': 'usdt_infolist_car'
    }

    s = requests.session()

    callbacked = os.popen('node callback.js')
    global callback, _
    callback = callbacked.readlines()[0].strip()
    _ = callback.split('_')[1]

    url_session = f'https://callback.58.com/antibot/codev2/getsession.do'
    resp_session = s.post(url_session, headers=headers_session, data=data_session).json()

    session_id = resp_session['data']['sessionId']  # 获取sessionid
    url_pic = f'https://verifycode.58.com/captcha/getV3?callback={callback}&_={_}&showType=win&sessionId={session_id}'
    resp_captcha = s.get(url_pic, headers=headers_captcha).text
    print(resp_captcha)

    resp = json.loads(re.findall(r'jQuery\d+.\d+\((.*?)\)', resp_captcha, re.S)[0])
    response_id = resp['data']['responseId']     # 获取responseid
    url_bgImg = 'https://verifycode.58.com' + str(resp['data']['bgImgUrl'])
    pic = s.get(url_bgImg).content
    with open('./imgs/1.png', 'wb') as f:
        f.write(pic)
    return s, session_id, response_id

def handle_img(threshold=140):
    """处理图片，进行灰度转换和二值化处理"""
    img = Image.open('./imgs/1.png')
    img = img.resize((300,169),Image.ANTIALIAS)
    img = img.convert('L')
    table = []
    for i in range(256):
        if i < threshold:
            table.append(0)
        else:
            table.append(1)
    img = img.point(table, '1')
    img.save('./imgs/handled.png')

def crop_img():
    """剪切图片获取子图"""
    img = Image.open('./imgs/small.png')
    box = (2,2,8,14)
    small = img.crop(box)
    small.save('./imgs/croped.png')

def match_template(confidencevalue=0.6):
    """模板匹配，找到滑块坐标"""
    croped = ac.imread('./imgs/croped.png')
    big = ac.imread('./imgs/handled.png')
    match_result = ac.find_template(big, croped, confidencevalue)
    if match_result is not None:
            match_result['shape'] = (big.shape[1], big.shape[0])  
    return match_result['result'][0]

def get_data(loc, response_id):
    """获取滑动轨迹加密结果"""
    ret = os.popen(f'node car_verify.js {loc} {response_id}')
    data = ret.readlines()[0].strip()
    return data

def check(s, response_id, session_id, data):
    """发送验证码校验请求"""
    url = f'https://verifycode.58.com/captcha/checkV3?callback={callback}&responseId={response_id}&sessionId={session_id}&data={data}&_={_}'
    headers_check = {
                    'referer': 'https://callback.58.com/antibot/verifycode?serialId=52e2ca88845f157e29a6d26349ef0344_6a059b1477814bb8baf7ee04e2b61764&code=22&sign=235448960cb8f4d6cd710b06c61dc57a&namespace=usdt_infolist_car&url=https%3A%2F%2Finfo5.58.com%3A443%2Ftj%2Fershouche%2F%3FPGTID%3D0d100000-0001-2e0e-cd43-870ea87150b4%26ClickID%3D2',
                    'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
                    'Cookie': 'id58=c5/nfF8oqage5rsmEt8aAg==; 58home=tj; city=tj; 58tj_uuid=f0606c8f-bfe0-4918-a05c-54792979825d; als=0; wmda_new_uuid=1; wmda_uuid=8d04e2a5433c9b87989e8dd97f0910d7; xxzl_deviceid=hk4tMTLXOoZDMq1OWS%2FbYdQsr1%2F6tJGVp6JnDKghAebk%2BWYpclsNs7e5ckbFgJ6m; wmda_visited_projects=%3B11187958619315%3B1732038237441; myLat=""; myLon=""; mcity=tj; new_uv=9; init_refer=https%253A%252F%252Fcallback.58.com%252Fantibot%252Fverifycode%253FserialId%253D52e2ca88845f157e29a6d26349ef0344_6a059b1477814bb8baf7ee04e2b61764%2526code%253D22%2526sign%253D235448960cb8f4d6cd710b06c61dc57a%2526namespace%253Dusdt_infolist_car%2526url%253Dhttps%25253A%25252F%25252Finfo5.58.com%25253A443%25252Ftj%25252Fershouche%25252F%25253FPGTID%25253D0d100000-0001-2e0e-cd43-870ea87150b4%252526ClickID%25253D2; wmda_session_id_11187958619315=1597305356023-87e5e97d-4b43-b080; new_session=0; xxzl_cid=279a85402ac4446f9b35cada3a75d9e8; xzuid=32ea41f7-b931-49f6-bda8-2b622227d207; spm=u-2d2yxv86y3v43nkddh1.BDPCPZ_BT; utm_source=link',
                    'Accept': '*/*',
                    'Sec-Fetch-Site': 'same-site',
                    'Sec-Fetch-Mode': 'no-cors',
                    'Sec-Fetch-Dest': 'script'
                        }
    resp = s.get(url, headers=headers_check)
    print(resp.text)

def verify():
    s, session_id, response_id = save_captcha()
    handle_img()
    loc = match_template() - 10 # 滑块实际要比传入加密js的坐标大10
    print(loc)
    data = get_data(loc, response_id)
    check(s, response_id, session_id, data)

if __name__ == "__main__":
    # crop_img()
    verify()
    


