
import logging
import getpass
import sys


class SpiderLog(object):

    def __new__(cls):
        if not hasattr(cls, '_instance'):
            cls._instance = super(SpiderLog, cls).__new__(cls)
        return cls._instance

    def __init__(self):
        self.user = getpass.getuser()
        self.logger = logging.getLogger(self.user)
        self.logger.setLevel(logging.DEBUG)
        self.logFile = sys.argv[0][0:-3] + '.log' # 日志文件名
        self.formatter = logging.Formatter(
            '%(asctime)-12s %(levelname)-8s %(name)-10s %(message)-12s\r\n')

        self.logHand = logging.FileHandler(self.logFile, encoding='utf8') # 输出到日志文件
        self.logHand.setFormatter(self.formatter)
        self.logHand.setLevel(logging.DEBUG)

        self.logger.addHandler(self.logHand) # 添加Handler

    def info(self, msg):
        self.logger.info(msg)

  

if __name__ == '__main__':
    spiderlog = SpiderLog()
    spiderlog.info("test")

