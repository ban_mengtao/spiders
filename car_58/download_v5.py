import requests
import re
import base64
from fontTools.ttLib import TTFont
from hashlib import md5
from bs4 import BeautifulSoup
import csv
import aiohttp
import asyncio
from city import city_list
from pybloom_live import BloomFilter
from log_write import SpiderLog
from car_verify import verify
from threading import Lock


class CarSpider:

    def __init__(self):
        self.lock = Lock()
        self.fail_url = []
        self.fail_count = 0
        self.flag = True
        self.spiderlog = SpiderLog()
        self.bf = BloomFilter(capacity=100000, error_rate=0.01)
        self.url_template = 'https://{addr}.58.com/ershouche/pn{page}'
        # 坐标和实际字体数据的映射关系，坐标经过md5处理
        self.dic_font = {'856c80c30a9c2100282e94be2ef01a1a': 3, '4c12e2ca6ab31a1832549d3a2661cee9': 2, '221ce0f06ec2094938778887f59c096c': 1, '0edc309270450f4e144f1fa90a633a72': 0, 'a06d9a83fde2ea9b2fd4b8c0e92da4d9': 7,
                    'fe91949296531c26783936c17da4c896': 6, '0d0fd3a2d04e61526662b13c2db00537': 5, '0958ad9f2976dce5451697bef0227a0f': 4, 'bf3f23b53cb12e04d67b3f141771508d': 9, '9de9732e406d7025c0005f2f9cec817a': 8}
        self.headers = {
            'Origin': 'https://tj.58.com',
            'Referer': 'https://c.58cdn.com.cn/escstatic/upgrade/zhuzhan_pc/ershouche/ershouche_list_v20200622145811.css',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.129 Safari/537.36'
        }

    async def downHTML(self, url, session):
        try:
            count = 0
            await asyncio.sleep(5)
            async with session.get(url, headers=self.headers) as resp:
                if str(resp.status)[0] == '2':
                    if url not in self.bf:
                        # 用布隆过滤器对url去重
                        self.bf.add(url)
                        self.spiderlog.info('正在爬取:{url}'.format(url=url))
                        await asyncio.sleep(3)
                        resp_text = await resp.text()
                        resp_text = await self.verify(resp_text, url)
                        return resp_text
                else:
                    count += 1
                    if count < 3:
                        await self.downHTML(url, session)
                return None
        except Exception as e:
            self.spiderlog.info(e)

    # 确认返回的页面是验证码页面还是数据页面
    async def verify(self, html, url):
        soup = BeautifulSoup(html, 'lxml')
        title = soup.select('title')[0].get_text()
        if '请输入验证码' in title:
            self.lock.acquire()
            if self.flag:   
                print('需要验证')
                verify()
                self.flag = False
            elif self.fail_count == 40:
                self.flag = True
                self.fail_count = 0
            self.fail_count += 1
            self.spiderlog.info(str(url)+'未爬取')
            self.fail_url.append(url)
            self.lock.release()
            return None
        else:
            return html

    # 58二手车有字体反爬，每次请求获取字体文件，提取字体坐标，和提前准备的字体坐标进行映射
    def getTempdict(self, html):
        pattern = re.compile(r"charset=utf-8;base64,(.*?)//wAP", re.S)
        try:
            ttf_url = re.search(pattern, html)
            if not ttf_url:
                return None
            content = base64.b64decode(ttf_url.group(1)+'=')
            with open('tc.ttf', 'wb') as f:
                f.write(content)

            font = TTFont('tc.ttf')
            temp_dict = {}
            for i, k in enumerate(font.getGlyphOrder()):
                if i == 0:
                    continue
                # 获取字体的坐标
                coor = font['glyf'][k].coordinates
                m = md5(str(coor).encode()).hexdigest()
                k = k.lower().replace('uni00', '&#x')
                k = k.replace('uni', '&#x')
                # 根据映射，将unicode编码替换为真实的数据
                temp_dict[k.lower()] = self.dic_font[m]
            return temp_dict
        except Exception as e:
            self.spiderlog.info(e)

    # 解析网页得到数据
    def parseHtml(self, html, temp_dict):
        soup = BeautifulSoup(html, 'lxml')
        if not temp_dict:
            return None
        for k, v in temp_dict.items():
            html = html.replace(k, str(v))
        try:           
            city = re.search(
                r'<title>【(.*?)二手车.*?二手车交易市场.*?58同城</title>', html).group(1)
            prices = soup.select('.info--price b')

            info = soup.select('.info_params')
            title = soup.select('.info_title>span')
            tag = soup.select('div.info--desc div:nth-of-type(1)')
        except Exception as e:
            self.spiderlog.info(e)
        for p, i, t, ta in zip(prices, info, title, tag):
            item = {}
            item['城市'] = city
            item['价格'] = p.get_text().replace(';', '')
            item['车型'] = t.get_text().split('急')[0].strip()
            i = i.get_text("\n", strip=True).split('\n')
            ta = '_'.join(ta.get_text().strip().split('\n'))
            item['上牌时间'] = i[0]
            item['里程'] = i[2]
            item['tag'] = ta
            yield item
     
    # 将数据安装格式保存为csv
    async def save(self, item):
        with open('car.csv', 'a', encoding='utf-8') as f:
            fieldname = ['城市', '价格', '车型', '上牌时间', '里程', 'tag']

            writer = csv.DictWriter(f, fieldnames=fieldname)
            writer.writerow({'城市': '城市', '价格': '价格(万)', '车型': '车型',
                             '上牌时间': '上牌时间', '里程': '里程', 'tag': 'tag'})
            for i in item:
                writer.writerow(i)

    async def main(self, url, seamphone):
        async with seamphone:
            async with aiohttp.ClientSession() as session:
                html_detail= await self.downHTML(url, session)
                if html_detail:
                    temp_dict = self.getTempdict(html_detail)
                    item = self.parseHtml(html_detail, temp_dict)
                    # await self.save(item)

    # 迭代生成url
    def generate_url(self):
        for a in city_list():
            for p in range(1, 70):
                yield self.url_template.format(addr=a, page=p)


if __name__ == "__main__":
    cs = CarSpider()

    seamphone = asyncio.Semaphore(200)
    loop = asyncio.get_event_loop()
    tasks = [asyncio.ensure_future(cs.main(url, seamphone)) for url in cs.generate_url()]
    loop.run_until_complete(asyncio.wait(tasks))

    
