# 登录后进行数据的爬取
import os
import aiohttp
import asyncio
import re
import json
from threading import Thread
import pymysql
from time import sleep


con = pymysql.Connect(host='localhost', user='root', port=3306, password='*******', database='taobao')
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.\
        105 Safari/537.36'}
cursor = con.cursor()

class ThreadLoop(Thread):
    """自定义线程类，以便于获取函数的返回值"""
    def __init__(self, func, loop):
        super(ThreadLoop, self).__init__()
        self.func = func
        self.loop = loop
    def get_result(self):
        return self.func(self.loop)

def generate_key():
    """从预先获取的淘宝导航栏关键字中取出一个并返回"""
    cursor.execute('select url from tb_url limit 0,10')
    for u in cursor.fetchall():
        yield u


async def login(s, password):
    """负责发送请求，登录校验,password:登录密码"""
    base_url = 'https://login.taobao.com/member/login.jhtml?spm=a21bo.2017.754894437.1.5af911d9KL7Th6&f=top&redirectURL=\
    https%3A%2F%2Fwww.taobao.com%2F'
    login_url = 'https://login.taobao.com/newlogin/login.do?appName=taobao&fromSite=0' # 进行登录校验的接口地址
    async with s.get(base_url, headers=headers) as resp:
        resp = await resp.text()
        token_pattern = re.compile(r'"umidToken":"(.*?)","hsiz"', re.S)
        hsiz_pattern = re.compile(r'"hsiz":"(.*?)","bizParams"', re.S)
        csrf_token_pattern = re.compile(r'"_csrf_token":"(.*?)","umidToken"', re.S)
        umidToken = re.findall(token_pattern, resp)[0]
        hsiz = re.findall(hsiz_pattern, resp)[0]
        csrf_token = re.findall(csrf_token_pattern, resp)[0]

        ret = os.popen(f'node tb.js {password}') # 获取密码的加密结果
        pwd = ret.readlines()[0].strip()
        data = {
            'loginId': 'phonenum',
            'password2': pwd,
            'keepLogin': 'false',
            'umidGetStatusVal': '255',
            'screenPixel': '1920x1080',
            'navlanguage': 'zh-CN',
            'navUserAgent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
            'navPlatform': 'Win32',
            'appName': 'taobao',
            'appEntrance': 'taobao_pc',
            '_csrf_token': csrf_token,
            'umidToken': umidToken,
            'hsiz': hsiz,
            'bizParams': '',
            'style': 'default',
            'appkey': '00000000',
            'from': 'tbTop',
            'isMobile': 'false',
            'lang': 'zh_CN',
            'returnUrl': 'https://www.taobao.com/',
            'fromSite': '0',
        }
        async with s.post(login_url, data=data, headers=headers) as resp_post:
            pass

async def login_entry():
    s = aiohttp.ClientSession()
    await login(s, 'password')
    return s

def start_loop(loop):
    """开启一个线程，专门负责进行登录"""
    asyncio.set_event_loop(loop)
    s = loop.run_until_complete(login_entry())
    return s

async def save_sql(booknames, prices, nicks, sales):
    """将数据保存到mysql，booknames:商品名，prices:价格，nicks:商铺名，sales:销量"""
    sql = 'insert into goods(name,price,nick,sales) value(%s,%s,%s,%s)'
    data = list(zip(booknames, prices, nicks[:-1], sales))
    try:
        cursor.executemany(sql, data)
        con.commit()
        print('正在存储.....')
    except Exception as e:
        print(e)
        con.rollback()


async def parse(html, s):
    """提取数据，booknames:商品名，prices:价格，nicks:商铺名，sales:销量"""
    try:
        name_pattern = re.compile(r'"raw_title":"(.*?)","pic_url"', re.S)
        booknames = re.findall(name_pattern, html)
        price_pattern = re.compile(r'"view_price":"(.*?)",', re.S)
        prices = re.findall(price_pattern, html)
        nick_pattern = re.compile(r'"nick":"(.*?)",', re.S)
        nicks = re.findall(nick_pattern, html)
        sales_pattern = re.compile(r'"view_sales":"(.*?)",',re.S)
        sales = re.findall(sales_pattern, html)
        if nicks and booknames and prices and sales:
            await save_sql(booknames, prices, nicks,sales)
        else:
            print('需要验证')
    except:
        print('解析有误')


async def fetch(s, url):
    """发送请求，获取网页源代码"""
    async with s.get(url, headers=headers, timeout=10) as resp_data:
        return await resp_data.text()


async def get_data(s, url, sem):
    async with sem:
        # sleep(5)
        html = await fetch(s, url)
        print('正在爬取：', url)
        await parse(html, s)
          
   
async def close_session(s):
    await s.close() 

def run():
    loop_thread = asyncio.new_event_loop() 
    a_thread = ThreadLoop(start_loop, loop_thread)
    a_thread.start()
    a_thread.join()
    s = a_thread.get_result()
    pages = [i for i in range(45, 200, 44)]
    data_url = 'https://s.taobao.com/search?q={key}&imgfile=&commend=all&ssid=s5-e&search_type=item&p4ppushleft=1%2C48&bcoffset=3&ntoffset=3&s={page}'
    loop = asyncio.get_event_loop()
    sem = asyncio.Semaphore(10, loop=loop) # 设置10个信号量
    tasks = [asyncio.ensure_future(get_data(s, data_url.format(page=i, key= key[0]), sem)) for key in generate_key() for i in pages]
    loop.run_until_complete(asyncio.wait(tasks))
    loop.run_until_complete(close_session(s))
    loop.close()
    con.close()


if __name__ == '__main__':
    run()




