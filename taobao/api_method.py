# 负责从mysql中获取商品关键字信息，从淘宝h5接口中请求数据
import requests
import pymysql
import os
import json
import time
from requests.utils import dict_from_cookiejar
import re
from urllib.parse import quote
from concurrent.futures import ThreadPoolExecutor
from functools import wraps


def retry(fun):
    """重试装饰器，默认重试三次"""
    @wraps(fun)
    def inner(self, kw, page):
        i = 0
        while i < 3:
            try:
                html = fun(self, kw, page)
                return html
            except:
                print('正在尝试重试')
            i += 1
        return None
    return inner


class TBSpider():

    def __init__(self):
        self.con = pymysql.Connect(host='localhost', user='root', port=3306, password='*****', database='taobao')
        self.cursor = self.con.cursor()
        self.s = requests.Session()
        self.headers_token = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
        }
        self.headers_api = {
            'referer': 'https://uland.taobao.com/sem/tbsearch?keyword=%E6%B7%98%E5%AE%9D%E7%BD%91%E5%8D%AB%E8%A1%A3%E7%94%B7%E5%A3%AB&page=2',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
        }
        self.cursor.execute('select url from tb_url limit 14,2')
        self.keys = self.cursor.fetchall()

    @retry
    def api(self, kw, page):
        """从淘宝h5接口中获取数据，并返回网页源代码
        :param kw:从数据库中获取的商品关键字
        :param page:获取的数据页码
        """
        kw_quote = '%22%3A%22' + quote(kw) + '%22%2C%22'
        ret_token = os.popen(f'node tb_token.js undefined {page} {kw}')
        res_token = ret_token.readlines()
        signtoken = res_token[0].strip()
        t_token = res_token[1].strip()
        url_token = f'https://h5api.m.taobao.com/h5/mtop.alimama.union.sem.landing.pc.items/1.0/?jsv=2.4.0&appKey=12574478&t={t_token}&sign={signtoken}&api=mtop.alimama.union.sem.landing.pc.items&v=1.0&AntiCreep=true&dataType=jsonp&type=jsonp&ecode=0&callback=mtopjsonp1&data=%7B%22keyword{kw_quote}ppath%22%3A%22%22%2C%22loc%22%3A%22%22%2C%22minPrice%22%3A%22%22%2C%22maxPrice%22%3A%22%22%2C%22ismall%22%3A%22%22%2C%22ship%22%3A%22%22%2C%22itemAssurance%22%3A%22%22%2C%22exchange7%22%3A%22%22%2C%22custAssurance%22%3A%22%22%2C%22b%22%3A%22%22%2C%22clk1%22%3A%22%22%2C%22pvoff%22%3A%22%22%2C%22pageSize%22%3A%22100%22%2C%22page%22%3A%222%22%2C%22elemtid%22%3A%221%22%2C%22refpid%22%3A%22%22%2C%22pid%22%3A%22430673_1006%22%2C%22featureNames%22%3A%22spGoldMedal%2CdsrDescribe%2CdsrDescribeGap%2CdsrService%2CdsrServiceGap%2CdsrDeliver%2C%20dsrDeliverGap%22%2C%22ac%22%3A%22SrXOF60ZPDwCAdpFSCiRyAER%22%2C%22wangwangid%22%3A%22%22%2C%22catId%22%3A%22%22%7D'
        resp_token = self.s.get(url_token, headers=self.headers_token)
        cookie = dict_from_cookiejar(resp_token.cookies)
        
        _m_h5_tk = cookie['_m_h5_tk'].split('_')[0]
        ret = os.popen(f'node tb_token.js {_m_h5_tk} {page} {kw}')
        res = ret.readlines()
        sign = res[0].strip()
        t = res[1].strip()
        url = f'https://h5api.m.taobao.com/h5/mtop.alimama.union.sem.landing.pc.items/1.0/?jsv=2.4.0&appKey=12574478&t={t}&sign={sign}&api=mtop.alimama.union.sem.landing.pc.items&v=1.0&AntiCreep=true&dataType=jsonp&type=jsonp&ecode=0&callback=mtopjsonp2&data=%7B%22keyword{kw_quote}ppath%22%3A%22%22%2C%22loc%22%3A%22%22%2C%22minPrice%22%3A%22%22%2C%22maxPrice%22%3A%22%22%2C%22ismall%22%3A%22%22%2C%22ship%22%3A%22%22%2C%22itemAssurance%22%3A%22%22%2C%22exchange7%22%3A%22%22%2C%22custAssurance%22%3A%22%22%2C%22b%22%3A%22%22%2C%22clk1%22%3A%22%22%2C%22pvoff%22%3A%22%22%2C%22pageSize%22%3A%22100%22%2C%22page%22%3A%222%22%2C%22elemtid%22%3A%221%22%2C%22refpid%22%3A%22%22%2C%22pid%22%3A%22430673_1006%22%2C%22featureNames%22%3A%22spGoldMedal%2CdsrDescribe%2CdsrDescribeGap%2CdsrService%2CdsrServiceGap%2CdsrDeliver%2C%20dsrDeliverGap%22%2C%22ac%22%3A%22SrXOF60ZPDwCAdpFSCiRyAER%22%2C%22wangwangid%22%3A%22%22%2C%22catId%22%3A%22%22%7D'
        resp_api = self.s.get(url, headers=self.headers_api).text
        if '非法请求' in resp_api:
            raise ValueError('重试')
        return resp_api

    def save_to_sql(self, data):
        """将解析后的数据存储到mysql"""
        sql = 'insert into goods(name,price,shop,sales,score) value(%s,%s,%s,%s,%s)'
        try:
            self.cursor.executemany(sql, data)
            self.con.commit()
            print('已存储')
        except Exception as e:
            print(e)
            self.con.rollback()

    def generate_key(self, keys):
        """从预先获取的淘宝导航栏关键字中取出一个并返回"""
        for u in keys:
            yield u

    def parse_data(self, future):
        """从网页源代码中提取数据，names:商品名,prices:商品价格,shops:店铺名,sales:销量,scores:店铺评分"""
        html = future.result()
        if html:
            data_all = re.findall(r'"data":(.*?),"ret"',html)[0]
            data = json.loads(data_all)['mainItems']
            shops = list(map(lambda x: x['wangwangId'], data))
            prices = list(map(lambda x: x['promoPrice'], data))
            names = list(map(lambda x: x['title'], data))
            sales = list(map(lambda x: x['sellCount'], data))
            scores = list(map(lambda x: x['dsrDeliver'], data))
            data_parsed = list(zip(names,prices,shops,sales,scores))
            self.save_to_sql(data_parsed)

    def main(self):
        pool = ThreadPoolExecutor(max_workers=3)
        for k in self.generate_key(self.keys):
            for p in range(1, 100):
                key = k[0]
                arg = [key, p]
                done = pool.submit(lambda x: self.api(*x), arg)
                done.add_done_callback(self.parse_data)
                time.sleep(4)
        self.con.close()

if __name__ == "__main__":
    tb_spider = TBSpider()
    tb_spider.main()


