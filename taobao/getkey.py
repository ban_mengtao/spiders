# 负责从淘宝主页获取商品的关键字信息，比如多肉，猫粮，内衣等，并存储到mysql

import os
import aiohttp
import asyncio
import re
import json
from threading import Thread
import threading
import pymysql
from bs4 import BeautifulSoup
import requests


headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.\
            105 Safari/537.36'}
con = pymysql.Connect(host='localhost', user='root', port=3306, password='password', database='taobao')
cursor = con.cursor()
class ThreadLoop(Thread):

    def __init__(self, func, loop):
        super(ThreadLoop, self).__init__()
        self.func = func
        self.loop = loop
    def get_result(self):
        return self.func(self.loop)

async def login(s, password):
    """负责发送请求，登录校验,password:登录密码"""
    base_url = 'https://login.taobao.com/member/login.jhtml?spm=a21bo.2017.754894437.1.5af911d9KL7Th6&f=top&redirectURL=\
    https%3A%2F%2Fwww.taobao.com%2F'
    login_url = 'https://login.taobao.com/newlogin/login.do?appName=taobao&fromSite=0'
    await asyncio.sleep(5)
    async with s.get(base_url, headers=headers) as resp:
        resp = await resp.text()
        token_pattern = re.compile(r'"umidToken":"(.*?)","hsiz"', re.S)
        hsiz_pattern = re.compile(r'"hsiz":"(.*?)","bizParams"', re.S)
        csrf_token_pattern = re.compile(r'"_csrf_token":"(.*?)","umidToken"', re.S)
        umidToken = re.findall(token_pattern, resp)[0]
        hsiz = re.findall(hsiz_pattern, resp)[0]
        csrf_token = re.findall(csrf_token_pattern, resp)[0]

        ret = os.popen(f'node ./taobao/tb.js {password}')
        pwd = ret.readlines()[0].strip()
        data = {
            'loginId': 'phonenum',
            'password2': pwd,
            'keepLogin': 'false',
            'umidGetStatusVal': '255',
            'screenPixel': '1920x1080',
            'navlanguage': 'zh-CN',
            'navUserAgent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
            'navPlatform': 'Win32',
            'appName': 'taobao',
            'appEntrance': 'taobao_pc',
            '_csrf_token': csrf_token,
            'umidToken': umidToken,
            'hsiz': hsiz,
            'bizParams': '',
            'style': 'default',
            'appkey': '00000000',
            'from': 'tbTop',
            'isMobile': 'false',
            'lang': 'zh_CN',
            'returnUrl': 'https://www.taobao.com/',
            'fromSite': '0',
        }
        async with s.post(login_url, data=data, headers=headers) as resp_post:
            pass

async def login_entry():
    s = aiohttp.ClientSession()
    await login(s, 'password')
    return s

def start_loop(loop):
    asyncio.set_event_loop(loop)
    s = loop.run_until_complete(login_entry())
    return s

async def save_sql(tags):
    """将淘宝主页关键字保存至mysql"""
    sql = 'insert into tb_url(url) value(%s)'
    try:
        cursor.executemany(sql, tags)
        con.commit()
        print('正在存储....')
    except Exception as e:
        print('存储失败+1')
        print(e)
        con.rollback()


def parse_keys():
    """从淘宝主页导航栏提取所有分类商品的链接"""
    url = 'https://fragment.tmall.com/tbhome/data?callback=siteMap_cb'
    html = requests.get(url, headers=headers).text
    links_pattern = re.compile(r'"link":\s*?"(.*?)",', re.S)
    for i in re.findall(links_pattern, html):
        yield i
    


async def parse_url(html):
    """从分类详情页中提取出导航栏具体商品的关键字"""
    def filter_tag(tag): # 负责筛选出关键字
        if tag.has_attr('href') and tag.string:
            if 1 < len(tag.string) < 6 and 'list' in tag.get('href'):
                return tag
            
    soup = BeautifulSoup(html, 'lxml')
    a_tags = soup.find_all(filter_tag)
    tags = list(map(lambda x:x.get_text(), a_tags[1:]))
    await save_sql(tags)


async def get_data(s, url, flag):
    try:
        async with s.get(url, headers=headers, timeout=3) as resp_data:
            print('正在爬取:', url)
            html = await resp_data.text()
            await parse_url(html)
    except Exception as e:
        print(e)

def main():
    loop_thread = asyncio.new_event_loop() 
    a_thread = ThreadLoop(start_loop, loop_thread)
    a_thread.start()
    a_thread.join()
    s = a_thread.get_result()
    links = parse_keys()
    loop_url = asyncio.get_event_loop()
    tasks = [asyncio.ensure_future(get_data(s, url, 'parse')) for url in links]
    loop_url.run_until_complete(asyncio.wait(tasks))
    loop_close = asyncio.get_event_loop()
    loop_close.run_until_complete(close_session(s))

async def close_session(s):
    await s.close() 

if __name__ == '__main__':
    main()
    
    








